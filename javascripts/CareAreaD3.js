var CareAreaD3 = function(selector, w, h, color) {
  this.w = w;
  this.h = h;

  d3.select(selector).selectAll("svg").remove();

  this.svg = d3
    .select(selector)
    .append("svg:svg")
    .attr("width", w)
    .attr("height", h);

  this.svg
    .append("svg:rect")
    .style("stroke", "transparent")
    .style("fill", "transparent")
    .attr("width", w)
    .attr("height", h);

  this.force = d3.layout
    .force()
    .on("tick", this.tick.bind(this))
    .charge(function(d) {
      return -20;
    })
    .linkDistance(function(d) {
      return w > 750
        ? d.target._children ? 180 : 100
        : d.target._children ? 100 : 40;
    })
    .size([h, w]);
};

CareAreaD3.prototype.update = function(json) {
  if (json) this.json = json;

  this.json.fixed = true;
  this.json.x = this.w / 2;
  this.json.y = this.h / 2;

  var nodes = this.flatten(this.json);
  var links = d3.layout.tree().links(nodes);
  var total = nodes.length || 1;

  // remove existing text (will readd it afterwards to be sure it's on top)
  this.svg.selectAll("text").remove();

  // Restart the force layout
  this.force
    .gravity(0)
    // .gravity(Math.atan(total / 50) / Math.PI * 0.4)
    .nodes(nodes)
    .links(links)
    .start();

  // Update the links
  this.link = this.svg.selectAll("line.link").data(links, function(d) {
    return d.target.name;
  });

  // Enter any new links
  this.link
    .enter()
    .insert("svg:line", ".node")
    .attr("class", "link")
    .attr("x1", d => d.source.x)
    .attr("y1", d => d.source.y)
    .attr("x2", d => d.target.x)
    .attr("y2", d => d.target.y);

  // Exit any old links.
  this.link.exit().remove();

  // Update the nodes
  this.node = this.svg
    .selectAll("circle.node")
    .data(nodes, d => d.name)
    .classed("collapsed", d => (d._children ? 1 : 0));

  this.node
    .transition()
    .attr(
      "r",
      d => (d.level === "category" ? 15 : d.level === "topic" ? 10 : 6)
    );

  // Enter any new nodes
  this.node
    .enter()
    .append("svg:circle")
    .attr("class", "node")
    .classed("directory", d => (d._children || d.children ? 1 : 0))
    .attr(
      "name",
      d =>
        `circle_${d.level}_${d.name.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, "")}`
    )
    .attr(
      "r",
      d => (d.level === "category" ? 15 : d.level === "topic" ? 10 : 6)
    )
    .style("fill", d => "#aaa")
    .call(this.force.drag)
    .on("click", this.click.bind(this))
    .on("mouseover", this.mouseover.bind(this))
    .on("mouseout", this.mouseout.bind(this));

  // Exit any old nodes
  this.node.exit().remove();

  //
  // Update the texts
  this.text = this.svg
    .selectAll("circle.nodetext")
    .data(nodes, d => d.name)
    .classed("collapsed", d => (d._children ? 1 : 0))
    .text(d => `${d.name}`)
    .attr("dy", d => d.x)
    .attr("dx", d => d.y)
    .attr("text-anchor", "middle")
    .attr("font-weight", d => (d.new_subtopic === "Y" ? "bold" : "normal"));

  this.text.transition().attr("r", d => (d.size > 20 ? 20 : 10));

  // Enter any new texts
  this.text
    .enter()
    .append("svg:text")
    .attr("class", "nodetext")
    .classed("directory", d => (d._children || d.children ? 1 : 0))
    .attr(
      "name",
      d => `text_${d.level}_${d.name.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, "")}`
    )
    .text(d => d.name)
    .attr("dy", d => (d.size > 20 ? 20 : 10) * -1.6)
    .attr("dx", d => 0)
    .attr("text-anchor", "middle")
    .attr("font-weight", d => (d.new_subtopic === "Y" ? "bold" : "normal"))
    .call(this.force.drag);
  //use <tspan> is not appear
  // .append("tspan")
  // .text(d => d.name)
  // .attr("dx", 0)
  // .attr("dy", 14)
  // .attr("fill", "#000");

  // Exit any old text
  this.text.exit().remove();

  return this;
};

CareAreaD3.prototype.flatten = function(root) {
  var nodes = [], i = 0;

  function recurse(node) {
    if (node.children) {
      node.size = node.children.reduce(function(p, v) {
        return p + recurse(v);
      }, 0);
    }
    if (!node.id) node.id = ++i;
    nodes.push(node);
    return node.size;
  }

  root.size = recurse(root);
  return nodes;
};

CareAreaD3.prototype.click = function clickclick(d) {
  //[N] opne topic's subtopic
  if ($(window).width() > 750) {
    if (d._children) {
      // Toggle children on click.
      d.children = d._children;
      d._children = null;
    } else {
      d._children = d.children;
      d.children = null;
    }
    this.update();
  }
};

CareAreaD3.prototype.mouseover = function(d) {
  $("text")
    .filter(function() {
      return $(this).text() === d.name;
    })
    .attr("font-weight", "bold");
};

CareAreaD3.prototype.mouseout = function(d) {
  d.new_subtopic === "Y"
    ? null
    : $("text")
        .filter(function() {
          return $(this).text() === d.name;
        })
        .attr("font-weight", "normal");
};

CareAreaD3.prototype.tick = function() {
  var h = this.h;
  var w = this.w;
  this.link
    .attr("x1", d => d.source.x)
    .attr("y1", d => d.source.y)
    .attr("x2", d => d.target.x)
    .attr("y2", d => d.target.y);

  this.node.attr("transform", function(d) {
    return `translate(${Math.max(5, Math.min(w - 5, d.x))},${Math.max(5, Math.min(h - 5, d.y))})`;
  });
  this.text
    .attr("transform", function(d) {
      return `translate(${Math.max(5, Math.min(w - 5, d.x))},${Math.max(5, Math.min(h - 5, d.y))})`;
    })
    .attr("text-anchor", d => (d.x > w / 2 ? "start" : "end"));
};

CareAreaD3.prototype.cleanup = function() {
  this.update([]);
  this.force.stop();
};
